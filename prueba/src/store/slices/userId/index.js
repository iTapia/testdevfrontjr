import { createSlice } from '@reduxjs/toolkit';
// axios
import axios from 'axios';

export const userIdSlice = createSlice({
    name: 'usersHook',
    initialState: {
        listUser: [],
    },
    reducers: {
        setShowUserHook: (state, action) => {
            state.listUser = action.payload;
        }
    },
});

export const {setShowUserHook} = userIdSlice.actions;
export default userIdSlice.reducer;

export const fetchAllUsersId = () => (dispatch) => {
    axios
        .get("https://jsonplaceholder.typicode.com/users/1")
        .then((response) => {
            dispatch(setShowUserHook(response.data));
        })
        .catch((error) => console.log(error))
}