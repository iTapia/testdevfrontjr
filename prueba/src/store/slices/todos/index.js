import { createSlice } from '@reduxjs/toolkit';
// axios
import axios from 'axios';

export const toDoSlice = createSlice({
    name: 'toDos',
    initialState: {
        list: [],
    },
    reducers: {
        setToDoList: (state, action) => {
            state.list = action.payload;
        }
    },
});

export const {setToDoList} = toDoSlice.actions;
export default toDoSlice.reducer;

export const fetchAllToDos = () => (dispatch) => {
    axios
        .get("https://jsonplaceholder.typicode.com/users/userid/todos")
        .then((response) => {
            dispatch(setToDoList(response.data));
        })
        .catch((error) => console.log(error))
}