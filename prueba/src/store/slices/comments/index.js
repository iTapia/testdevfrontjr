import { createSlice } from '@reduxjs/toolkit';
// axios
import axios from 'axios';

export const commentSlice = createSlice({
    name: 'comments',
    initialState: {
        list: [],
    },
    reducers: {
        setCommentList: (state, action) => {
            state.list = action.payload;
        }
    },
});

export const {setCommentList} = commentSlice.actions;
export default commentSlice.reducer;

export const fetchAllComments = () => (dispatch) => {
    axios
        .get("https://jsonplaceholder.typicode.com/post/(postId)/comments")
        .then((response) => {
            dispatch(setCommentList(response.data));
        })
        .catch((error) => console.log(error))
}