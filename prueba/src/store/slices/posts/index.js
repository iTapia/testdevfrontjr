import { createSlice } from '@reduxjs/toolkit';
// axios
import axios from 'axios';

export const postSlice = createSlice({
    name: 'posts',
    initialState: {
        list: [],
    },
    reducers: {
        setPostList: (state, action) => {
            state.list = action.payload;
        }
    },
});

export const {setPostList} = postSlice.actions;
export default postSlice.reducer;

export const fetchAllPosts = () => (dispatch) => {
    axios
        .get("https://jsonplaceholder.typicode.com/users/userid/posts")
        .then((response) => {
            dispatch(setPostList(response.data));
        })
        .catch((error) => console.log(error))
}