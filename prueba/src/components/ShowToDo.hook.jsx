import React, {useEffect} from 'react';
import{useDispatch, useSelector} from 'react-redux';
import {fetchAllToDos} from '../store/slices/todos';

const ShowToDoHook = () => {

    const {list : todos} = useSelector(state => state.todos);
    const dispatch = useDispatch();

    useEffect(() =>{
        dispatch(fetchAllToDos());
    }, [dispatch])

    return (
        <div>
            <button className = "btn btn-dark btn-sm mb-2" onClick = {() => {
                todos.map((todo, index) => (
                    <div key={index} className="mb-4 border border-dark">
                        <div className="card-body">
                            <h4 className="card-title" align="center">{`${todo.title}`}</h4>
                            <p className="card-text" align="center"> Realizado: {todo.completed}</p>
                        </div>
                    </div>
                ))
            }}>Mostrar tareas</button>
        </div>
    );
}
export default ShowToDoHook;