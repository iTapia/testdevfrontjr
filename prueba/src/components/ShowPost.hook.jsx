import React, {useEffect} from 'react';
import{useDispatch, useSelector} from 'react-redux';
import {fetchAllPosts} from '../store/slices/posts';

const ShowPostHook = () => {

    const {listPost : posts} = useSelector(state => state.posts);
    const dispatch = useDispatch();

    useEffect(() =>{
        dispatch(fetchAllPosts());
    }, [dispatch])

    return (
        <div>
            <button className = "btn btn-dark btn-sm mb-2" onClick = {() => {
                posts.map((post, index) => (
                    <div key={index} className="mb-4 border border-dark">
                        <div className="card-body">
                            <h4 className="card-title" align="center">Titulo: {`${post.title}`}</h4>
                            <p className="card-text" align="center">{post.body}</p>
                        </div>
                    </div>
                ))
            }}>Mostrar publicaciones</button>
        </div>
    );
}
export default ShowPostHook;