import React, {useEffect} from "react";
//redux
import {fetchAllUsers} from '../store/slices/users';
import {useDispatch, useSelector} from 'react-redux'

const UserList = () => {

    const {list : users} = useSelector(state => state.users);
    const dispatch = useDispatch();

    useEffect(() =>{
        dispatch(fetchAllUsers());
    }, [dispatch])

    return (
        <div className="container">
            <select>
                {users.map((user) => {
                    return <option key={user.id} value={user.id}>
                        {user.id} - {user.name}
                    </option>
                })}
            </select>
        </div>
        
    )
}

export default UserList