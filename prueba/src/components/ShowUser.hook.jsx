import React, {useEffect} from 'react';
//redux
import {fetchAllUsersId} from '../store/slices/userId';
import{useDispatch, useSelector} from 'react-redux';


const ShowUserHook = () => {

    const {listUser : usersHook} = useSelector(state => state.usersHook);
    const dispatch = useDispatch();

    useEffect(() =>{
        dispatch(fetchAllUsersId());
    }, [dispatch])

    return (
        <div>
            <button className = "btn btn-dark btn-sm mb-2" onClick = {() => {
                usersHook.map((user, index) => (
                    <div key={index} className="mb-4 border border-dark">
                        <div className="card-body">
                            <h4 className="card-title" align="center">{`${user.name}`}</h4>
                            <p className="card-text" align="center">{user.username} <br></br> {user.email} <br></br> {user.address.street}
                            <br></br> {user.phone} <br></br> {user.website} <br></br> {user.company.name} </p>
                        </div>
                    </div>
                ))
            }}>Mostrar informacion</button>
        </div>
    );
}

export default ShowUserHook;