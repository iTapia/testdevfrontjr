import React from 'react'
import Navbar from "./components/Navbar";
import UserList from './components/UserList';
import ShowUserHook from './components/ShowUser.hook';
import ShowPostHook from './components/ShowPost.hook';
import ShowToDoHook from './components/ShowToDo.hook';
// redux
import {Provider} from 'react-redux';
import store from './store';

function App(){
  return(
    <Provider store={store}>
      <Navbar />
      <div>
        <div className="row">
          <div className="col-12">
            <div className="card mt-5" style={{ maxwidth: '370x'}}>
              <div className="row no-gutters">
                <div className="card-body  text-center">
                  <div className="card-title">
                    <UserList />
                  </div>
                  <ShowUserHook />
                  <br></br>
                  <ShowPostHook />
                  <br></br>
                  <ShowToDoHook />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Provider>
  )
}
  

export default App;
